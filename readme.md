Information
===========
Project for install a full development environement for django apps.

Requirements
============
```
python3
- virtualenv
Django2
```

Installation guide for unix systems
===================================
```
virtualenv --no-setuptools MY_PROJECT
cd MY_PROJECT
source bin/activate
pip install zc.buildout
buildout
```

Django manage.py is accessible through ./bin/manage

Start server
============

Develop mode
```
./bin/manage runserver 127.0.0.1:8080
```

Productive mode
```
#Start server
./bin/uwsgi --ini ./parts/uwsgi/uwsgi.ini

#Stop server
./bin/uwsgi --stop ./var/server-master.pid
```
